def ftoc(int)
  ((int - 32) * 5.0 / 9)
end

def ctof(int)
  (int * 9.0 / 5) + 32
end
