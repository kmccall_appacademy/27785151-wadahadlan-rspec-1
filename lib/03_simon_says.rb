def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, repititions = 2)
  arr = []
  repititions.times do
    arr << str
  end
  arr.join(' ')
end

def start_of_word(str, num = 1)
  str.split('').take(num).join
end

def first_word(str)
  str.split[0]
end

def titleize(str)
  little_words = ['and', 'the', 'over', 'under']
  str.split.map.with_index do |word, idx|
    unless little_words.include?(word) && idx != 0
      word[0].upcase + word[1..-1]
    else
      word
    end
  end.join(' ')
end

p titleize("the bridge over the river kwai")
