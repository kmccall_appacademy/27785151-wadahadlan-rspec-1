def add(int1, int2)
  int1 + int2
end

def subtract(int1, int2)
  int1 - int2
end

def sum(arr)
  return arr.reduce(:+) if arr.empty? == false
  0
end

def multiply(arr)
  arr.reduce(:*)
end

def power(int1, int2) #no one man should have all that
  num = 1
  int2.times do
    num *= int1
  end
  num
end

def factorial(int)
  return 1 if int == 0
  (1..int).to_a.reduce(:*)
end
